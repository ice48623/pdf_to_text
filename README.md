# Batch PDF_TO_TEXT Service

## Description
An web application that accept tar-gzipped ('.tgz, .tar.gz') archive that contain one or more pdf files and generate an output as tar-gzipped ('.tgz, .tar.gz') archive where each PDF file has been converted into TEXT file.

## Features
- File Upload
- File Download
- Near real time status reporting
- Support multiple user
- Support scaling for each worker (extractor, converter, archiver)

## Get Started

### Prerequisite

For MacOSX
```
brew install kubectl
brew cask install virtualbox minikube
```

### Clone the project
```
git clone --recurse-submodules git@gitlab.com:ice48623/pdf_to_text.git
```

### Start minikube
```
minikube start
```

### Using kubernetes's docker env
```
eval $(minikube docker-env)
```

### Build docker images
```
pdf-to-text-service/k8s/build-images.sh
```

### Create kubernetes's deployment and services
```
pdf-to-text-service/k8s/k8s.sh
```

### Modify host file
Add `<minikbe IP> pdftotext.io` to host file

### Undoing all
```
pdf-to-text-service/k8s/k8s.sh -d
minikube stop
eval $(minikube docker-env -u)
```
remove `<minikbe IP> pdftotext.io` from host file

## Note

### To access kubernetes's dashboard
```
minikube dashboard
```

## Used technology
- Python Flask
- RabbitMQ
- Vuejs
- Go
- SocketIO
- Kubernetes
- Redis
- Minio

## Member
- [Thanaphat Teeradatchusuk](https://gitlab.com/ice48623)
- [Peter Son Struschka](https://gitlab.com/pstruschka)
